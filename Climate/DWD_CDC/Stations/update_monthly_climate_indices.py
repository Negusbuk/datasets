#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: Andreas Mussgiller
"""

import os
import configparser

import logging

import re
import zipfile
import tempfile
import urllib.request
import pandas as pd

class UpdateClimateIndices():

    @classmethod
    def downloadIndex(cls, basepath1, idx1, basepath2, idx2):
        source = basepath1
        with urllib.request.urlopen(source) as response:
            with open(idx1.name, mode="wb") as html:
                html.write(response.read())
                idx1.close()
        
        source = basepath2
        with urllib.request.urlopen(source) as response:
            with open(idx2.name, mode="wb") as html:
                html.write(response.read())
                idx2.close()
                        
    @classmethod
    def scanIndex(cls, idx1, stations1, idx2, stations2):
        with open(idx1.name) as f:
            for line in f:
                m = re.search('href="monatswerte_KLINDEX_(.+?)_(.+?)_(.+?)_hist.zip"', line)
                if m:
                    station = int(m.group(1))
                    start = m.group(2)
                    end = m.group(3)
                    found = 'monatswerte_KLINDEX_{:05d}_{:05s}_{:05s}_hist.zip'.format(station, start, end)
                    stations1[station] = { 'file' : found, 'start' : start, 'end' : end }
        idx1.close()
        os.unlink(idx1.name)
    
        with open(idx2.name) as f:
            for line in f:
                m = re.search('href="monatswerte_KLINDEX_(.+?)_akt.zip"', line)
                if m:
                    station = int(m.group(1))
                    found = 'monatswerte_KLINDEX_{:05d}_akt.zip'.format(station)
                    stations2[station] = { 'file' : found }
        idx2.close()
        os.unlink(idx2.name)
    
    @classmethod
    def downloadandprocess(cls, basepath1, stations1, basepath2, stations2, station):
            
        logger = logging.getLogger()    
        
        logger.info(f'update station {station}')
    
        converters = {
            'MESS_DATUM_BEGINN': lambda x: pd.to_datetime(x, format='%Y%m%d'),
            'MESS_DATUM_ENDE': lambda x: pd.to_datetime(x, format='%Y%m%d')
        }
        
        tmp = tempfile.NamedTemporaryFile(delete=False, suffix='.zip')
        #print(tmp.name)
        tmpdir = tempfile.TemporaryDirectory()
        #print(tmpdir.name)
        
        source = basepath1 + stations1[station]['file']
        #print(source)
        
        with urllib.request.urlopen(source) as response:
            with open(tmp.name, mode="wb") as d:
                d.write(response.read())
                tmp.close()
        
        with zipfile.ZipFile(tmp.name, 'r') as zip_ref:
            zip_ref.extractall(tmpdir.name)
            tmp.close()
            os.unlink(tmp.name)
        
        start = stations1[station]['start']
        end = stations1[station]['end']
        source = tmpdir.name + '/produkt_klindex_monat_{:s}_{:s}_{:05d}.txt'.format(start, end, station)
        #print(source)
        
        data = pd.read_csv(source,
            encoding='utf-8', sep=';',
            converters=converters)
                
        logger.info('historic data read')
        
        if station in stations2.keys():
            
            tmp = tempfile.NamedTemporaryFile(delete=False, suffix='.zip')
            #print(tmp.name)
            tmpdir = tempfile.TemporaryDirectory()
            #print(tmpdir.name)
        
            source = basepath2 + stations2[station]['file']
            #print(source)
        
            with urllib.request.urlopen(source) as response:
                with open(tmp.name, mode="wb") as d:
                    d.write(response.read())
                    tmp.close()
                
            with zipfile.ZipFile(tmp.name, 'r') as zip_ref:
                zip_ref.extractall(tmpdir.name)
                tmp.close()
                os.unlink(tmp.name)
            
            source = ''
            for path in os.scandir(tmpdir.name):
                if path.is_file():
                    #print(path.name)
                    if path.name.startswith('produkt'):
                        source = tmpdir.name + '/' + path.name
            #print(source)
        
            if source!='':
                data2 = pd.read_csv(source,
                    encoding='utf-8', sep=';',
                    converters=converters)
                data = pd.concat([data, data2])
                        
            logger.info('recent data read')        
        
        data.fillna(0, inplace=True)
        
        data.rename(columns = {
            'MESS_DATUM_BEGINN': 'Start',
            'MESS_DATUM_ENDE': 'End',
            'MO_TROPENNAECHTE': 'Tropennächte',
            'MO_FROSTTAGE': 'Frosttage',
            'MO_SOMMERTAGE': 'Sommertage',
            'MO_HEISSE_TAGE': 'HeisseTage',
            'MO_EISTAGE': 'Eistage'
        }, inplace=True)
        
        data.drop_duplicates(subset=['Start'], keep='last', inplace=True)
        
        #datetime_index = pd.DatetimeIndex(data['Start'].values)
        #data.set_index(datetime_index, inplace=True)
        
        data.drop([
            'STATIONS_ID',
            'QN_4',
            'eor'
        ], axis=1, inplace=True)
        
        data.to_csv('monthly_climate_indices_{:05d}.csv'.format(station),
                    encoding='utf-8', mode='w', index=False)
        #print(data.tail())
                        
        logger.info('data written')
    
    @classmethod
    def update(cls):
            
        logger = logging.getLogger()
        
        logger.info('updateClimateIndices')
            
        basepath1 = 'https://opendata.dwd.de/climate_environment/CDC/observations_germany/climate/monthly/climate_indices/kl/historical/'
        basepath2 = 'https://opendata.dwd.de/climate_environment/CDC/observations_germany/climate/monthly/climate_indices/kl/recent/'
        
        stations1 = {}
        stations2 = {}
        
        idx1 = tempfile.NamedTemporaryFile(delete=False, suffix='.html')
        idx2 = tempfile.NamedTemporaryFile(delete=False, suffix='.html')
    
        UpdateClimateIndices.downloadIndex(basepath1, idx1,
                                          basepath2, idx2)
        UpdateClimateIndices.scanIndex(idx1, stations1,
                                      idx2, stations2)
        
        UpdateClimateIndices.downloadandprocess(basepath1, stations1,
                                               basepath2, stations2,
                                               1975) # Hamburg-Fuhlsbüttel
        UpdateClimateIndices.downloadandprocess(basepath1, stations1,
                                               basepath2, stations2,
                                               3379) # München-Stadt

if __name__ == "__main__":
    
    configParser = configparser.RawConfigParser()
    configParser.read(os.path.expanduser('~/.accounts.ini'))

    UpdateClimateIndices.update()
