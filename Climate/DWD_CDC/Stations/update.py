#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: Andreas Mussgiller
"""

import os
import configparser

from update_stations import UpdateStations
from update_monthly import UpdateMonthly
from update_monthly_climate_indices import UpdateClimateIndices
from update_monthly_precipitation import UpdatePrecipitation

if __name__ == "__main__":
    
    configParser = configparser.RawConfigParser()
    configParser.read(os.path.expanduser('~/.accounts.ini'))

    UpdateStations.update()
    UpdateMonthly.update()
    UpdateClimateIndices.update()
    UpdatePrecipitation.update()
