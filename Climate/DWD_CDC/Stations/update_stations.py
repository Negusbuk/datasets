#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: Andreas Mussgiller
"""

import os
import configparser

import logging

import tempfile
import urllib.request
import pandas as pd

class UpdateStations():

    @classmethod
    def update(cls):
            
        logger = logging.getLogger()
        
        logger.info('UpdateStations::update')
    
        source = 'https://opendata.dwd.de/climate_environment/CDC/observations_germany/climate/monthly/kl/historical/KL_Monatswerte_Beschreibung_Stationen.txt'
        tmp = tempfile.NamedTemporaryFile(delete=False, suffix='.txt')
        
        with urllib.request.urlopen(source) as response:
            with open(tmp.name, mode="wb") as txt:
                txt.write(response.read())
                tmp.close()
        
        logger.info('data read')
    
        entries = []
        linecount = 0
        with open(tmp.name, 'r', encoding='latin') as file:
            while (line := file.readline()):
                linecount += 1
                if linecount<=2:
                    continue
                
                row = []
                trow = line.split()
                #print(trow)
                
                row.append(int(trow.pop(0))) # Station ID
                row.append(pd.to_datetime(trow.pop(0), format='%Y%m%d')) # Start
                row.append(pd.to_datetime(trow.pop(0), format='%Y%m%d')) # End
                row.append(int(trow.pop(0))) # Height
                row.append(float(trow.pop(0))) # Latitude
                row.append(float(trow.pop(0))) # Longitude
                
                state = trow.pop()
                name = ' '.join(trow)
        
                row.append(name)
                row.append(state)
            
                #print(row)
                
                entries.append(row)
        
            tmp.close()
            os.unlink(tmp.name)
        
        columns = [
            'StationID',
            'Start',
            'End',
            'Height',
            'Latitude',
            'Longitude',
            'StationName',
            'State'
        ]
        
        data = pd.DataFrame(entries, columns=columns)
        
        #print(data.head())
        #print(data.tail())
        
        data.to_csv('stations.csv',
                    encoding='utf-8', mode='w', index=False)
            
        logger.info('data written')

if __name__ == "__main__":
    
    configParser = configparser.RawConfigParser()
    configParser.read(os.path.expanduser('~/.accounts.ini'))

    UpdateStations.update()

