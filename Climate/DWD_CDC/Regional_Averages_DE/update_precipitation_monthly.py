#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: Andreas Mussgiller
"""

import os
import configparser

import logging

import re
import zipfile
import tempfile
import urllib.request
import pandas as pd

class UpdateRegionalPrecipitation():
    
    @classmethod
    def update(cls):
                    
        logger = logging.getLogger()
        
        logger.info('UpdateRegionalPrecipitation::update')
        
        basepath = 'https://opendata.dwd.de/climate_environment/CDC/regional_averages_DE/monthly/precipitation/'
        
        dfs = []
        
        for m in range(1,13):
        	
        	converters = {'Jahr': lambda x: pd.to_datetime(x, format='%Y')}
        	
        	source = basepath + 'regional_averages_rr_{:02d}.txt'.format(m)
        	#print(source)
        	
        	tmp = tempfile.NamedTemporaryFile(delete=False, suffix='.txt')
        	#print(tmp.name)
        	
        	with urllib.request.urlopen(source) as response:
        		with open(tmp.name, mode="wb") as d:
        			d.write(response.read())
        			tmp.close()
        	
        	df = pd.read_csv(tmp.name,
        		encoding='utf-8', sep=';', decimal='.', skiprows=1,
        		converters=converters)
        	df.drop(['Monat', 'Unnamed: 19'], axis=1, inplace=True)
        	df['Jahr'] = df['Jahr'].apply(lambda x: x.replace(month=m, day=1))
        	dfs.append(df)
        	tmp.close()
        	os.unlink(tmp.name)
        
        
        data = pd.concat(dfs)
        
        data.sort_values(by=['Jahr'], inplace=True)
        
        data.rename(columns={'Jahr': 'Date'}, errors='raise', inplace=True)
        
        data.to_csv('precipitation_monthly.csv', encoding='utf-8', mode='w', index=False)
        
        #print(data.head())
        #print(data.tail())
        
        logger.info('data written')

if __name__ == "__main__":
    
    configParser = configparser.RawConfigParser()
    configParser.read(os.path.expanduser('~/.accounts.ini'))

    UpdateRegionalPrecipitation.update()
