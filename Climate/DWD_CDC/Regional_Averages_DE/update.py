#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: Andreas Mussgiller
"""

import os
import configparser

from update_temperature_monthly import UpdateRegionalTemperatures
from update_sunshine_duration_monthly import UpdateRegionalSunshineDuration
from update_precipitation_monthly import UpdateRegionalPrecipitation

if __name__ == "__main__":
    
    configParser = configparser.RawConfigParser()
    configParser.read(os.path.expanduser('~/.accounts.ini'))

    UpdateRegionalTemperatures.update()
    UpdateRegionalSunshineDuration.update()
    UpdateRegionalPrecipitation.update()
