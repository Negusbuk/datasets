import os
import re
import zipfile
import shutil
import tempfile
import urllib.request
import datetime
from functools import reduce
import pandas as pd
import numpy as np

# source: National Oceanic and Atmospheric Administration (NOAA)
# https://gml.noaa.gov/ccgg

colnames=['year', 'month', 'decimal', 'average_CO2', 'deseasonalized_CO2', 'days', 'std', 'unc']

df = pd.read_csv('archive/co2_mm_mlo.txt', header=None,
	names=colnames,
	encoding='utf-8', delim_whitespace=True,
	decimal='.', comment='#')
df.set_index(['year','month'], drop=False, inplace=True)
df.drop(['year', 'month', 'decimal', 'days', 'std', 'unc'], axis=1, inplace=True)

df.reset_index(drop=False, inplace=True)
df['day'] = 1
df['date'] = pd.to_datetime(df[['year', 'month', 'day']])

df = df[['date', 'average_CO2', 'deseasonalized_CO2']]

df.to_csv('data_mauna_loa.csv', encoding='utf-8', mode='w', index=False)

colnamesDaily=['year', 'month', 'day', 'decimal', 'CO2']

df = pd.read_csv('archive/co2_daily_mlo.txt', header=None,
	names=colnamesDaily,
	encoding='utf-8', delim_whitespace=True,
	decimal='.', comment='#')
df.set_index(['year','month', 'day'], drop=False, inplace=True)
df.drop(['year', 'month', 'day', 'decimal'], axis=1, inplace=True)

df.reset_index(drop=False, inplace=True)
df['date'] = pd.to_datetime(df[['year', 'month', 'day']])

df = df[['date', 'CO2']]

df.to_csv('data_mauna_loa_daily.csv', encoding='utf-8', mode='w', index=False)
