import os
import re
import zipfile
import shutil
import tempfile
import urllib.request
import datetime
from functools import reduce
import pandas as pd
import numpy as np

# source: National Oceanic and Atmospheric Administration (NOAA)
# https://gml.noaa.gov/ccgg

# Lan, X., K.W. Thoning, and E.J. Dlugokencky
# Trends in globally-averaged CH4, N2O, and SF6 determined from NOAA Global Monitoring Laboratory measurements.
# Version 2023-01,
# https://doi.org/10.15138/P8XG-AA10

# Lan, X., Tans, P. and K.W. Thoning
# Trends in globally-averaged CO2 determined from NOAA Global Monitoring Laboratory measurements.
# Version 2023-01 NOAA/GML (gml.noaa.gov/ccgg/trends/)

colnames=['year', 'month', 'decimal', 'average', 'average_unc', 'trend', 'trend_unc']

df_co2 = pd.read_csv('archive/co2_mm_gl.txt', header=None,
	names=colnames,
	encoding='utf-8', delim_whitespace=True,
	decimal='.', comment='#')
df_co2.set_index(['year','month'], drop=False, inplace=True)
df_co2.drop(['year', 'month', 'decimal', 'trend', 'trend_unc'], axis=1, inplace=True)
df_co2.rename(columns={ 'average': 'CO2', 'average_unc': 'CO2_unc' },
	inplace=True)

df_ch4 = pd.read_csv('archive/ch4_mm_gl.txt', header=None,
	names=colnames,
	encoding='utf-8', delim_whitespace=True,
	decimal='.', comment='#')
df_ch4.set_index(['year','month'], drop=False, inplace=True)
df_ch4.drop(['year', 'month', 'decimal', 'trend', 'trend_unc'], axis=1, inplace=True)
df_ch4.rename(columns={ 'average': 'CH4', 'average_unc': 'CH4_unc' },
	inplace=True)

df_n2o = pd.read_csv('archive/n2o_mm_gl.txt', header=None,
	names=colnames,
	encoding='utf-8', delim_whitespace=True,
	decimal='.', comment='#')
df_n2o.set_index(['year','month'], drop=False, inplace=True)
df_n2o.drop(['year', 'month', 'decimal', 'trend', 'trend_unc'], axis=1, inplace=True)
df_n2o.rename(columns={ 'average': 'N2O', 'average_unc': 'N2O_unc' },
	inplace=True)

df_sf6 = pd.read_csv('archive/sf6_mm_gl.txt', header=None,
	names=colnames,
	encoding='utf-8', delim_whitespace=True,
	decimal='.', comment='#')
df_sf6.set_index(['year','month'], drop=False, inplace=True)
df_sf6.drop(['year', 'month', 'decimal', 'trend', 'trend_unc'], axis=1, inplace=True)
df_sf6.rename(columns={ 'average': 'SF6', 'average_unc': 'SF6_unc' },
	inplace=True)

dfs = [df_co2, df_ch4, df_n2o, df_sf6]
df = reduce(lambda l,r: pd.merge(l,r, how='outer',
	left_index=True, right_index=True), dfs)

df.reset_index(drop=False, inplace=True)
df['day'] = 1
df['date'] = pd.to_datetime(df[['year', 'month', 'day']])

df = df[['date', 'CO2', 'CO2_unc', 'CH4', 'CH4_unc', 'N2O', 'N2O_unc', 'SF6', 'SF6_unc']]

df.to_csv('data_global.csv', encoding='utf-8', mode='w', index=False)
