import os
import re
import zipfile
import shutil
import tempfile
import urllib.request
import datetime
from functools import reduce
import pandas as pd
import numpy as np

# source: National Oceanic and Atmospheric Administration (NOAA)
# https://www.ncei.noaa.gov/products/paleoclimatology
# https://www.ncei.noaa.gov/pub/data/paleo/icecore/antarctica

colnames=['age_gas_calBP', 'co2_ppm', 'co2_1s_ppm']

df = pd.read_csv('archive/antarctica2015co2composite.txt', header=None,
	names=colnames,
	encoding='utf-8', delim_whitespace=True,
	skiprows=138,
	decimal='.', comment='#')

year = 1950 - df.age_gas_calBP

df['year'] = year

df = df[['year', 'age_gas_calBP', 'co2_ppm', 'co2_1s_ppm']]

df.to_csv('antarctica2015co2composite.csv', encoding='utf-8', mode='w', index=False)
