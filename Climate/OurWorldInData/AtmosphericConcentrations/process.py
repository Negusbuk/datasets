import os
import re
import zipfile
import shutil
import tempfile
import urllib.request
import datetime
import pandas as pd
import numpy as np

# source: https://ourworldindata.org/atmospheric-concentrations

# data: National Oceanic and Atmospheric Administration (NOAA)
# https://gml.noaa.gov/ccgg/trends/global.html

# Hannah Ritchie, Max Roser and Pablo Rosado (2020)
# "CO₂ and Greenhouse Gas Emissions". Published online at OurWorldInData.org.
# Retrieved from: 'https://ourworldindata.org/co2-and-other-greenhouse-gas-emissions'

# BibTex
#@article{owidco2andothergreenhousegasemissions,
#	author = {Hannah Ritchie and Max Roser and Pablo Rosado},
#	title = {CO₂ and Greenhouse Gas Emissions},
#	journal = {Our World in Data},
#	year = {2020},
#	note = {https://ourworldindata.org/co2-and-other-greenhouse-gas-emissions}
#}

df = pd.read_csv('archive/climate-change.csv',
	encoding='utf-8', sep=',', decimal='.', header=0)

Entities = df.Entity.unique()
Entities = ['World']
#print(Entities)

df.rename(columns={
	'CO2 concentrations': 'CO2',
	'CH4 concentrations': 'CH4',
	'N2O concentrations': 'N2O'
}, inplace=True)

df = df[['Entity', 'Year', 'CO2', 'CH4', 'N2O']]
#print(df.head())

for entity in Entities:
	fentity = entity.replace(' ', '_')

	df_CO2 = df.query('Entity=="{:s}" and CO2.isnull()==False'.format(entity))
	df_CO2 = df_CO2[['Year', 'CO2']]
	df_CO2.rename(columns={'CO2': 'Concentration'}, inplace=True)
	df_CO2.to_csv('{:s}_CO2.csv'.format(fentity), encoding='utf-8', mode='w', index=False)
	
	df_CH4 = df.query('Entity=="{:s}" and CH4.isnull()==False'.format(entity))
	df_CH4 = df_CH4[['Year', 'CH4']]
	df_CH4.rename(columns={'CH4': 'Concentration'}, inplace=True)
	df_CH4.to_csv('{:s}_CH4.csv'.format(fentity), encoding='utf-8', mode='w', index=False)
	
	df_N2O = df.query('Entity=="{:s}" and N2O.isnull()==False'.format(entity))
	df_N2O = df_N2O[['Year', 'N2O']]
	df_N2O.rename(columns={'N2O': 'Concentration'}, inplace=True)
	df_N2O.to_csv('{:s}_N2O.csv'.format(fentity), encoding='utf-8', mode='w', index=False)
