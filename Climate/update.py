import os
import sys
import subprocess

print(sys.executable)

basedir = os.getcwd()
print(basedir)

scripts = {
	'DWD_CDC/Stations': [
		'update_stations.py',
		'update_monthly.py',
		'update_monthly_climate_indices.py',
		'update_monthly_precipitation.py'
	],
	'DWD_CDC/Regional_Averages_DE': [
		'update_precipitation_monthly.py',
		'update_sunshine_duration_monthly.py',
		'update_temperature_monthly.py'
	]
}

for k,v in scripts.items():
	os.chdir(k)
	print(k)
	for s in v:
		print(' - ' + s)
		cmd = [sys.executable, s]
		shell_cmd = subprocess.run((cmd), capture_output=True, text=True)
		command_output=(shell_cmd.stdout)
		print(command_output)
		
	os.chdir(basedir)
	