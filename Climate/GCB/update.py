import pandas as pd
import numpy as np
import json

data = pd.read_excel('archive/National_Fossil_Carbon_Emissions_2023v1.0.xlsx',
	sheet_name='Territorial Emissions',
	skiprows=11, header=0)

data['World'] = data['World']-data['Statistical Difference']
data.drop(['Statistical Difference'], axis=1, inplace=True)

data.rename(columns = { 'Unnamed: 0': 'Year' }, inplace=True)
#print(temp.head())

data.to_csv('territorial_emissions.csv', encoding='utf-8', mode='w', index=False)

regions = data.columns.unique().values
regions = regions[1:]

countries = [1] * np.where(regions=='KP Annex B')[0][0]
countries.extend([0] * (1+np.where(regions=='World')[0][0]-np.where(regions=='KP Annex B')[0][0]))

#fields = [ { 'name': 'Year', 'type': 'date', 'format': '%Y' } ]
fields = [ { 'name': 'Year', 'type': 'integer' } ]

for r in regions:
	fields.append({ 'name': r, 'type': 'number'})
	
schema = { 'fields': fields }

#print(json.dumps(schema, indent=2))

with open('schema_territorial_emissions.json', 'w') as f:
	f.write(json.dumps(schema, indent=2))
	f.close()
	
datareg = pd.read_excel('archive/National_Fossil_Carbon_Emissions_2022v1.0.xlsx',
	sheet_name='Regions',
	skiprows=0, header=None, names=['Region', 'Countries'])
datareg.set_index('Region', inplace=True)

composition = []
for idx,region in enumerate(regions):
	if countries[idx]==1 or region=='Bunkers' or region=='Statistical Difference' or region=='World':
		composition.append(region)
	else:
		comp = datareg.loc[region].Countries
		comp = comp.replace('Bonaire, Saint Eustatius and Saba', 'BES')
		clist = comp.split(",")
		clist = [x.strip(' ') for x in clist]
		
		if 'BES' in clist:
			clist[clist.index('BES')] = 'Bonaire, Saint Eustatius and Saba'
		
		composition.append(';'.join(clist))

regions = pd.DataFrame(list(zip(regions, countries, composition)),
	columns =['Region', 'IsCountry', 'Composition'])
#print(regions.head())

regions.to_csv('regions.csv', encoding='utf-8', mode='w', index=False)
