import pandas as pd
import numpy as np

data = pd.read_csv('archive/UNSD — Methodology.csv',
  encoding='utf-8', sep=';',
  header=[0])
data.fillna(0, inplace=True)
  
data.loc[(data['Least Developed Countries (LDC)'] == 'x'),
  'Least Developed Countries (LDC)'] = 1
data.loc[(data['Land Locked Developing Countries (LLDC)'] == 'x'),
  'Land Locked Developing Countries (LLDC)'] = 1
data.loc[(data['Small Island Developing States (SIDS)'] == 'x'),
  'Small Island Developing States (SIDS)'] = 1

data.loc[(data['M49 Code'] == 680), 'ISO-alpha2 Code'] = 'RK'
data.loc[(data['M49 Code'] == 680), 'ISO-alpha3 Code'] = 'ARK'

data.loc[(data['M49 Code'] == 10), 'Region Name'] = 'Antarctica'
data.loc[(data['M49 Code'] == 10), 'Sub-region Name'] = 'Antarctica'

#print(data['M49 Code'])

d = {
  'ISO-alpha3 Code': data['ISO-alpha3 Code'].tolist(),
  'ISO-alpha2 Code': data['ISO-alpha2 Code'].tolist(),
  'M49 Code': data['M49 Code'].tolist(),
  'Country or Area Name': data['Country or Area'].tolist(),
  'Region Name': data['Region Name'].tolist(),
  'Sub-region Name': data['Sub-region Name'].tolist()
}
data = pd.DataFrame(data=d)

d = pd.Series({
  'ISO-alpha3 Code': 'TWN',
  'ISO-alpha2 Code': 'TW',
  'M49 Code': 158,
  'Country or Area Name': 'Republic of China',
  'Region Name': 'Asia',
  'Sub-region Name': 'Eastern Asia'
})
data = pd.concat([data, d.to_frame().T], ignore_index=True)

d = pd.Series({
  'ISO-alpha3 Code': 'XXK',
  'ISO-alpha2 Code': 'XK',
  'M49 Code': 412,
  'Country or Area Name': 'Kosovo',
  'Region Name': 'Europe',
  'Sub-region Name': 'Eastern Europe'
})
data = pd.concat([data, d.to_frame().T], ignore_index=True)

data.sort_values('ISO-alpha3 Code', inplace=True)

data.to_csv('m49standard.csv', index=None);

d = {
  'Country': data['Country or Area Name'].tolist(),
  'ISO3166': data['ISO-alpha3 Code'].tolist(),
  'IsShortname': [True]*len(data['ISO-alpha3 Code'].tolist())
}

data = pd.DataFrame(data=d)

data = pd.concat([data, pd.Series({
  'Country': 'Taiwan',
  'ISO3166': 'TWN',
  'IsShortname': True
}).to_frame().T], ignore_index=True)

data = pd.concat([data, pd.Series({
  'Country': 'Russia',
  'ISO3166': 'RUS',
  'IsShortname': True
}).to_frame().T], ignore_index=True)

data = pd.concat([data, pd.Series({
  'Country': 'Iran',
  'ISO3166': 'IRN',
  'IsShortname': True
}).to_frame().T], ignore_index=True)

data = pd.concat([data, pd.Series({
  'Country': 'South Korea',
  'ISO3166': 'KOR',
  'IsShortname': True
}).to_frame().T], ignore_index=True)

data = pd.concat([data, pd.Series({
  'Country': 'North Korea',
  'ISO3166': 'PKR',
  'IsShortname': True
}).to_frame().T], ignore_index=True)

data = pd.concat([data, pd.Series({
  'Country': 'Czech Republic',
  'ISO3166': 'CZE',
  'IsShortname': True
}).to_frame().T], ignore_index=True)

data = pd.concat([data, pd.Series({
  'Country': 'Turkey',
  'ISO3166': 'TUR',
  'IsShortname': True
}).to_frame().T], ignore_index=True)

data = pd.concat([data, pd.Series({
  'Country': 'Turkiye',
  'ISO3166': 'TUR',
  'IsShortname': False
}).to_frame().T], ignore_index=True)

data = pd.concat([data, pd.Series({
  'Country': 'Venezuela',
  'ISO3166': 'VEN',
  'IsShortname': True
}).to_frame().T], ignore_index=True)

data = pd.concat([data, pd.Series({
  'Country': 'Venezuela, RB',
  'ISO3166': 'VEN',
  'IsShortname': False
}).to_frame().T], ignore_index=True)

data = pd.concat([data, pd.Series({
  'Country': 'Egypt, Arab Rep.',
  'ISO3166': 'EGY',
  'IsShortname': False
}).to_frame().T], ignore_index=True)

data = pd.concat([data, pd.Series({
  'Country': 'Vietnam',
  'ISO3166': 'VNM',
  'IsShortname': True
}).to_frame().T], ignore_index=True)

data = pd.concat([data, pd.Series({
  'Country': 'Slovak Republic',
  'ISO3166': 'SVK',
  'IsShortname': False
}).to_frame().T], ignore_index=True)

data = pd.concat([data, pd.Series({
  'Country': 'Syria',
  'ISO3166': 'SYR',
  'IsShortname': True
}).to_frame().T], ignore_index=True)

data = pd.concat([data, pd.Series({
  'Country': 'Hong Kong SAR, China',
  'ISO3166': 'HKG',
  'IsShortname': False
}).to_frame().T], ignore_index=True)

data = pd.concat([data, pd.Series({
  'Country': 'Hong Kong',
  'ISO3166': 'HKG',
  'IsShortname': True
}).to_frame().T], ignore_index=True)

data = pd.concat([data, pd.Series({
  'Country': 'Kyrgyz Republic',
  'ISO3166': 'KGZ',
  'IsShortname': False
}).to_frame().T], ignore_index=True)

data = pd.concat([data, pd.Series({
  'Country': 'Yemen, Rep.',
  'ISO3166': 'YEM',
  'IsShortname': False
}).to_frame().T], ignore_index=True)

data = pd.concat([data, pd.Series({
  'Country': 'Curacao',
  'ISO3166': 'CUW',
  'IsShortname': False
}).to_frame().T], ignore_index=True)

data = pd.concat([data, pd.Series({
  'Country': 'Cote d’Ivoire',
  'ISO3166': 'CIV',
  'IsShortname': False
}).to_frame().T], ignore_index=True)

data = pd.concat([data, pd.Series({
  'Country': 'Cote d\'Ivoire',
  'ISO3166': 'CIV',
  'IsShortname': False
}).to_frame().T], ignore_index=True)

data = pd.concat([data, pd.Series({
  'Country': 'Côte d\'Ivoire',
  'ISO3166': 'CIV',
  'IsShortname': False
}).to_frame().T], ignore_index=True)

data = pd.concat([data, pd.Series({
  'Country': 'Congo, Dem. Rep.',
  'ISO3166': 'COD',
  'IsShortname': False
}).to_frame().T], ignore_index=True)

data = pd.concat([data, pd.Series({
  'Country': 'Dem. Rep. Congo',
  'ISO3166': 'COD',
  'IsShortname': True
}).to_frame().T], ignore_index=True)

data = pd.concat([data, pd.Series({
  'Country': 'United States',
  'ISO3166': 'USA',
  'IsShortname': False
}).to_frame().T], ignore_index=True)

data = pd.concat([data, pd.Series({
  'Country': 'USA',
  'ISO3166': 'USA',
  'IsShortname': True
}).to_frame().T], ignore_index=True)

data = pd.concat([data, pd.Series({
  'Country': 'Bonaire, Saint Eustatius and Saba',
  'ISO3166': 'BES',
  'IsShortname': True
}).to_frame().T], ignore_index=True)

data = pd.concat([data, pd.Series({
  'Country': 'Cape Verde',
  'ISO3166': 'CPV',
  'IsShortname': False
}).to_frame().T], ignore_index=True)

data = pd.concat([data, pd.Series({
  'Country': 'Faeroe Islands',
  'ISO3166': 'FRO',
  'IsShortname': False
}).to_frame().T], ignore_index=True)

data = pd.concat([data, pd.Series({
  'Country': 'Laos',
  'ISO3166': 'LAO',
  'IsShortname': True
}).to_frame().T], ignore_index=True)

data = pd.concat([data, pd.Series({
  'Country': 'Macao',
  'ISO3166': 'MAC',
  'IsShortname': True
}).to_frame().T], ignore_index=True)

data = pd.concat([data, pd.Series({
  'Country': 'Occupied Palestinian Territory',
  'ISO3166': 'PSE',
  'IsShortname': False
}).to_frame().T], ignore_index=True)

data = pd.concat([data, pd.Series({
  'Country': 'Bolivia',
  'ISO3166': 'BOL',
  'IsShortname': True
}).to_frame().T], ignore_index=True)

data = pd.concat([data, pd.Series({
  'Country': 'Moldova',
  'ISO3166': 'MDA',
  'IsShortname': True
}).to_frame().T], ignore_index=True)

data = pd.concat([data, pd.Series({
  'Country': 'Swaziland',
  'ISO3166': 'SWZ',
  'IsShortname': False
}).to_frame().T], ignore_index=True)

data = pd.concat([data, pd.Series({
  'Country': 'United Kingdom',
  'ISO3166': 'GBR',
  'IsShortname': False
}).to_frame().T], ignore_index=True)

data = pd.concat([data, pd.Series({
  'Country': 'UK',
  'ISO3166': 'GBR',
  'IsShortname': True
}).to_frame().T], ignore_index=True)

data = pd.concat([data, pd.Series({
  'Country': 'Tanzania',
  'ISO3166': 'TZA',
  'IsShortname': True
}).to_frame().T], ignore_index=True)

data.loc[(data['Country'] == 'Bonaire, Sint Eustatius and Saba'), 'IsShortname'] = False
data.loc[(data['Country'] == 'Czechia'), 'IsShortname'] = False
data.loc[(data['Country'] == 'United Kingdom of Great Britain and Northern Ireland'), 'IsShortname'] = False
data.loc[(data['Country'] == 'China, Hong Kong Special Administrative Region'), 'IsShortname'] = False
data.loc[(data['Country'] == 'Iran (Islamic Republic of)'), 'IsShortname'] = False
data.loc[(data['Country'] == 'Republic of Korea'), 'IsShortname'] = False
data.loc[(data['Country'] == 'Lao People\'s Democratic Republic'), 'IsShortname'] = False
data.loc[(data['Country'] == 'China, Macao Special Administrative Region'), 'IsShortname'] = False
data.loc[(data['Country'] == 'Republic of Moldova'), 'IsShortname'] = False
data.loc[(data['Country'] == 'Russian Federation'), 'IsShortname'] = False  
data.loc[(data['Country'] == 'Republic of China'), 'IsShortname'] = False
data.loc[(data['Country'] == 'Venezuela (Bolivarian Republic of)'), 'IsShortname'] = False
data.loc[(data['Country'] == 'Viet Nam'), 'IsShortname'] = False
data.loc[(data['Country'] == 'United States of America'), 'IsShortname'] = False
data.loc[(data['Country'] == 'Democratic Republic of the Congo'), 'IsShortname'] = False
data.loc[(data['Country'] == 'United Republic of Tanzania'), 'IsShortname'] = False

data.sort_values('ISO3166', inplace=True)

data.to_csv('iso3166map.csv', index=None);

#print(data.loc[(data['ISO3166'] == 'SPM'), 'Country'])

#for index,row in data.iterrows():
#  print('{:s} {:d}: {:s}'.format(row.ISO3166, row.IsShortname, row.Country))
  