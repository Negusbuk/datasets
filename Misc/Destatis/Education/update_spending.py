import pandas as pd
import numpy as np

keys = {}
entries = []

headerline = 1
with open('archive/21711-0001_flat.csv',
          encoding='ISO-8859-1') as file:
    for line in file:
        if headerline==1:
            headers = line.rstrip().replace(',', '.').split(';')
            
            keys['Jahr'] = headers.index('Zeit')
            keys['Land'] = headers.index('1_Auspraegung_Label')
            keys['Körperschaftsgruppe'] = headers.index('2_Auspraegung_Label')
            keys['Aufgabenbereich'] = headers.index('3_Auspraegung_Label')
            keys['Ausgaben'] = headers.index('ASG028__Ausgaben_der_oeffentlichen_Haushalte_fuer_Bildung__Tsd._EUR')
            keys['AusgabenProEinwohner'] = headers.index('ASG029__Ausgaben_der_oeff._Haushalte_fuer_Bildung_je_Einw.__EUR')
            keys['AnteilBIP'] = headers.index('ASG030__Anteil_d.Ausgaben_d._oeff._Haushalte_f.Bild._am_BIP__Prozent')
            keys['AnteilGesamthaushalt'] = headers.index('ASG031__Anteil_d.Ausgaben_d._oeff._HH_f.Bild._am_GesamtHH__Prozent')
            
            headerline = 0
            continue
        
        tokens = line.rstrip().replace(',', '.').split(';')
        for i in range(len(tokens)):
            if tokens[i]=='.':
                tokens[i] = '0.0'
         
        row = []
        
        row.append(pd.to_numeric(tokens[keys['Jahr']]))
        row.append(tokens[keys['Land']])
        row.append(tokens[keys['Körperschaftsgruppe']])
        row.append(tokens[keys['Aufgabenbereich']])
        row.append(1000.0*pd.to_numeric(tokens[keys['Ausgaben']]))
        row.append(pd.to_numeric(tokens[keys['AusgabenProEinwohner']]))
        row.append(pd.to_numeric(tokens[keys['AnteilBIP']]))
        row.append(pd.to_numeric(tokens[keys['AnteilGesamthaushalt']]))
        
        entries.append(row)
        
        #print(row)
        #break

df0001 = pd.DataFrame(entries, columns=keys)
#print(df0001.head())

keys = {}
entries = []

headerline = 1
with open('archive/21711-0010_flat.csv',
          encoding='ISO-8859-1') as file:
    for line in file:
        if headerline==1:
            headers = line.rstrip().replace(',', '.').split(';')
            
            keys['Jahr'] = headers.index('Zeit')
            keys['Land'] = headers.index('1_Auspraegung_Label')
            keys['Körperschaftsgruppe'] = headers.index('2_Auspraegung_Label')
            keys['Aufgabenbereich'] = headers.index('3_Auspraegung_Label')
            keys['Ausgaben'] = headers.index('ASG028__Ausgaben_der_oeffentlichen_Haushalte_fuer_Bildung__Tsd._EUR')
            keys['AusgabenProEinwohner'] = headers.index('ASG029__Ausgaben_der_oeff._Haushalte_fuer_Bildung_je_Einw.__EUR')
            keys['AnteilBIP'] = headers.index('ASG030__Anteil_d.Ausgaben_d._oeff._Haushalte_f.Bild._am_BIP__Prozent')
            keys['AnteilGesamthaushalt'] = headers.index('ASG031__Anteil_d.Ausgaben_d._oeff._HH_f.Bild._am_GesamtHH__Prozent')
            
            headerline = 0
            continue
        
        tokens = line.rstrip().replace(',', '.').split(';')
        for i in range(len(tokens)):
            if tokens[i]=='.':
                tokens[i] = '0.0'
         
        row = []
        
        row.append(pd.to_numeric(tokens[keys['Jahr']]))
        row.append(tokens[keys['Land']])
        row.append(tokens[keys['Körperschaftsgruppe']])
        row.append(tokens[keys['Aufgabenbereich']])
        row.append(1000.0*pd.to_numeric(tokens[keys['Ausgaben']]))
        row.append(pd.to_numeric(tokens[keys['AusgabenProEinwohner']]))
        row.append(pd.to_numeric(tokens[keys['AnteilBIP']]))
        row.append(pd.to_numeric(tokens[keys['AnteilGesamthaushalt']]))
        
        entries.append(row)
        
        #print(row)
        #break

df0010 = pd.DataFrame(entries, columns=keys)
#print(df0010.head())

df = pd.concat([df0001, df0010])
#print(df.head())

df.to_csv('spending.csv', encoding='utf-8', mode='w', index=False)
