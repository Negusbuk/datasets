import pandas as pd
import numpy as np
import json

JahrgangsstufenMap = {
    'Insgesamt': 'Insgesamt',
    'Keine Angabe': 'Keine Angabe',
    'Klassenstufe 1': 'KS1',
    'Klassenstufe 2': 'KS2',
    'Klassenstufe 3': 'KS3',
    'Klassenstufe 4': 'KS4',
    'Klassenstufe 5': 'KS5',
    'Klassenstufe 6': 'KS6',
    'Klassenstufe 7': 'KS7',
    'Klassenstufe 8': 'KS8',
    'Klassenstufe 9': 'KS9',
    'Klassenstufe 10': 'KS10',
    'Jahrgangsstufe 11 / Einführungsphase E': 'JS11E',
    'Jahrgangsstufe 12 / Qualifizierungsphase Q1': 'JS12Q1',
    'Jahrgangsstufe 13 / Qualifizierungsphase Q2': 'JS13Q2'
}

GeschlechterMap = {
    'männlich': 'M',
    'weiblich': 'W',
    'Insgesamt': 'T'
}

keys = {}
entries = []

headerline = 1
with open('archive/21111-0011_flat.csv',
          encoding='ISO-8859-1') as file:
    for line in file:
        if headerline==1:
            headers = line.rstrip().replace(',', '.').split(';')
            
            #print(headers)
            
            keys['Schuljahr'] = headers.index('Zeit')
            keys['SchuljahrStart'] = headers.index('Zeit')
            keys['Land'] = headers.index('1_Auspraegung_Label')
            keys['iLand'] = headers.index('1_Auspraegung_Code')
            keys['Schulart'] = headers.index('2_Auspraegung_Label')
            keys['iSchulart'] = headers.index('2_Auspraegung_Code')
            keys['Jahrgangsstufe'] = headers.index('3_Auspraegung_Label')
            keys['iJahrgangsstufe'] = headers.index('3_Auspraegung_Code')
            keys['Geschlecht'] = headers.index('4_Auspraegung_Label')
            keys['iGeschlecht'] = headers.index('4_Auspraegung_Code')
            keys['Anzahl'] = headers.index('BIL001__Schueler__Anzahl')
              
            headerline = 0
            continue
        
        tokens = line.rstrip().replace(',', '.').split(';')
        
        if tokens[keys['Anzahl']]=='-':
            #tokens[keys['Anzahl']] = '0'
            continue
        
        if tokens[keys['iJahrgangsstufe']]=='':
            tokens[keys['iJahrgangsstufe']] = 'STUFE99'

        if tokens[keys['iGeschlecht']]=='':
            tokens[keys['iGeschlecht']] = 'GESX'
        
        row = []
        
        SchuljahrTokens = tokens[keys['Schuljahr']].rstrip().split('/')
        SchuljahrStart = pd.to_numeric(SchuljahrTokens[0])
          
        row.append(tokens[keys['Schuljahr']])
        row.append(SchuljahrStart)
        row.append(tokens[keys['Land']])
        row.append(pd.to_numeric(tokens[keys['iLand']]))
        row.append(tokens[keys['Schulart']])
        row.append(tokens[keys['iSchulart']])
        row.append(JahrgangsstufenMap[tokens[keys['Jahrgangsstufe']]])
        row.append(tokens[keys['iJahrgangsstufe']])
        row.append(GeschlechterMap[tokens[keys['Geschlecht']]])
        row.append(tokens[keys['iGeschlecht']])
        row.append(pd.to_numeric(tokens[keys['Anzahl']]))
    
        entries.append(row)
        
        #print(row)
        #break

df = pd.DataFrame(entries, columns=keys)
#print(df.head())

#Schuljahre = df.Schuljahr.unique()
#Schularten = df.Schulart.unique()
#Jahrgangsstufen = df.Jahrgangsstufe.unique()
#Geschlechter = df.Geschlecht.unique()

#print(Schuljahre)
#print(Schularten)
#print(Jahrgangsstufen)
#print(Geschlechter)

grouped = df.groupby(['Schuljahr',
                      'SchuljahrStart',
                      'Schulart',
                      'iSchulart',
                      'Jahrgangsstufe',
                      'iJahrgangsstufe',
                      'Geschlecht',
                      'iGeschlecht'], as_index=False).sum()

entries = []

for idx,row in grouped.iterrows():
    newrow = []
    newrow.append(row['Schuljahr'])
    newrow.append(row['SchuljahrStart'])
    newrow.append('Deutschland')
    newrow.append(99)
    newrow.append(row['Schulart'])
    newrow.append(row['iSchulart'])
    newrow.append(row['Jahrgangsstufe'])
    newrow.append(row['iJahrgangsstufe'])
    newrow.append(row['Geschlecht'])
    newrow.append(row['iGeschlecht'])
    newrow.append(row['Anzahl'])
    entries.append(newrow)

dfDE = pd.DataFrame(entries, columns=keys)

data = pd.concat([df, dfDE], ignore_index=True)

data = data.sort_values(by=['SchuljahrStart',
                            'iLand',
                            'iSchulart',
                            'iJahrgangsstufe',
                            'iGeschlecht'])

data = data.drop(columns=['iSchulart', 'iJahrgangsstufe', 'iGeschlecht'])

data.to_csv('students.csv', encoding='utf-8', mode='w', index=False)

#print(data.head())

columns = data.columns.tolist()

fields = []
for c in columns:
    fields.append({ 'name': c, 'type': 'string'})
fields[1]['type'] = 'integer'
fields[-1]['type'] = 'integer'

schema = { 'fields': fields }

#print(json.dumps(schema, indent=2))

with open('schema_students.json', 'w') as f:
    f.write(json.dumps(schema, indent=2))
    f.close()
  