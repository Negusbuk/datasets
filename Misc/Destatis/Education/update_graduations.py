import pandas as pd
import numpy as np
import json

GeschlechterMap = {
    'männlich': 'M',
    'weiblich': 'W',
    'Insgesamt': 'T'
}

keys = {}
entries = []

headerline = 1
with open('archive/21111-0013_flat.csv',
          encoding='ISO-8859-1') as file:
    for line in file:
        if headerline==1:
            headers = line.rstrip().replace(',', '.').split(';')
            
            #print(headers)
            
            keys['Schuljahr'] = headers.index('Zeit')
            keys['SchuljahrStart'] = headers.index('Zeit')
            keys['Land'] = headers.index('1_Auspraegung_Label')
            keys['iLand'] = headers.index('1_Auspraegung_Code')
            keys['Schulart'] = headers.index('2_Auspraegung_Label')
            keys['iSchulart'] = headers.index('2_Auspraegung_Code')
            keys['Abschluss'] = headers.index('3_Auspraegung_Label')
            keys['iAbschluss'] = headers.index('3_Auspraegung_Code')
            keys['Geschlecht'] = headers.index('4_Auspraegung_Label')
            keys['iGeschlecht'] = headers.index('4_Auspraegung_Code')
            keys['Anzahl'] = headers.index('BILABS__Absolventen_und_Abgaenger__Anzahl')
              
            headerline = 0
            continue
        
        tokens = line.rstrip().replace(',', '.').split(';')
        
        if tokens[keys['Anzahl']]=='-':
            #tokens[keys['Anzahl']] = '0'
            continue
        
        if tokens[keys['iAbschluss']]=='':
            tokens[keys['iAbschluss']] = 'BILABS15'

        if tokens[keys['iGeschlecht']]=='':
            tokens[keys['iGeschlecht']] = 'GESX'
        
        row = []
        
        SchuljahrTokens = tokens[keys['Schuljahr']].rstrip().split('/')
        SchuljahrStart = pd.to_numeric(SchuljahrTokens[0])
          
        row.append(tokens[keys['Schuljahr']])
        row.append(SchuljahrStart)
        row.append(tokens[keys['Land']])
        row.append(pd.to_numeric(tokens[keys['iLand']]))
        row.append(tokens[keys['Schulart']])
        row.append(tokens[keys['iSchulart']])
        row.append(tokens[keys['Abschluss']])
        row.append(tokens[keys['iAbschluss']])
        row.append(GeschlechterMap[tokens[keys['Geschlecht']]])
        row.append(tokens[keys['iGeschlecht']])
        row.append(pd.to_numeric(tokens[keys['Anzahl']]))
        
        entries.append(row)
        
        #print(row)
        #break

df = pd.DataFrame(entries, columns=keys)
#print(df.head())

#Schuljahre = df.Schuljahr.unique()
#Schularten = df.Schulart.unique()
#Jahrgangsstufen = df.Abschluss.unique()
#Geschlechter = df.Geschlecht.unique()

#print(Schuljahre)
#print(Schularten)
#print(Jahrgangsstufen)
#print(Geschlechter)

grouped = df.groupby(['Schuljahr',
                      'SchuljahrStart',
                      'Schulart',
                      'iSchulart',
                      'Abschluss',
                      'iAbschluss',
                      'Geschlecht',
                      'iGeschlecht'], as_index=False).sum()

entries = []

for idx,row in grouped.iterrows():
    
    newrow = []
    newrow.append(row['Schuljahr'])
    newrow.append(row['SchuljahrStart'])
    newrow.append('Deutschland')
    newrow.append(99)
    newrow.append(row['Schulart'])
    newrow.append(row['iSchulart'])
    newrow.append(row['Abschluss'])
    newrow.append(row['iAbschluss'])
    newrow.append(row['Geschlecht'])
    newrow.append(row['iGeschlecht'])
    newrow.append(row['Anzahl'])
    entries.append(newrow)

dfDE = pd.DataFrame(entries, columns=keys)

data = pd.concat([df, dfDE], ignore_index=True)

data = data.sort_values(by=['SchuljahrStart',
                            'iLand',
                            'iSchulart',
                            'iAbschluss',
                            'iGeschlecht'])

data = data.drop(columns=['iSchulart', 'iAbschluss', 'iGeschlecht'])

data.to_csv('graduations.csv', encoding='utf-8', mode='w', index=False)

#print(data.head())

columns = data.columns.tolist()

fields = []
for c in columns:
    fields.append({ 'name': c, 'type': 'string'})
fields[1]['type'] = 'integer'
fields[-1]['type'] = 'integer'

schema = { 'fields': fields }

#print(json.dumps(schema, indent=2))

with open('schema_graduations.json', 'w') as f:
    f.write(json.dumps(schema, indent=2))
    f.close()
