#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: Andreas Mussgiller
"""

import locale
locale.setlocale(locale.LC_ALL, 'en_US.UTF-8')

import os
import configparser
import json

import logging


import requests

from io import StringIO
import pandas as pd

def updateRealWageIndex(token):

    logging.basicConfig(format="{asctime} [{levelno}:{name}]: {message}",
                        style="{",
                        datefmt="%Y-%m-%d %H:%M",
                        level=logging.INFO)
    logger = logging.getLogger()
    
    logger.info('reading catalogue/tables for 62361-0010')
    
    url = 'https://www-genesis.destatis.de/genesisWS/rest/2020/catalogue/tables'
    params = dict(
        username = token,
        selection = '62361-0010',
        area = 'all',
        pagelength = '20',
        compress = 'false',
        language = 'en'
    )
    
    response = requests.get(url=url, params=params)
    data = response.json()
    if 'Type' in data:
        if data['Type']=='ERROR':
            print(data['Content'])
            return
    #print(json.dumps(data, indent=4))

    entry = data['List'][0]
    match = entry['Time'].split(' to ')
    temp = match[0].strip().lstrip('Quarter').strip().split(' ')
    quarterStart = int(float(temp[0].strip()))
    yearStart = int(float(temp[1].strip()))
    temp = match[1].strip().lstrip('Quarter').strip().split(' ')
    quarterEnd = int(float(temp[0].strip()))
    yearEnd = int(float(temp[1].strip()))
    
    logger.info(f'range: Q{quarterStart}/{yearStart} - Q{quarterEnd}/{yearEnd}')

    logger.info('reading data/table for 62361-0010')
    
    url = 'https://www-genesis.destatis.de/genesisWS/rest/2020/data/table'
    params = dict(
        username = token,
        name = '62361-0010',
        startyear = f'{yearStart}',
        endyear = f'{yearEnd}',
        job = 'false',
        values = 'true',
        metadata = 'true',
        area = 'all',
        compress = 'false',
        language = 'en',
    )
    
    response = requests.get(url=url, params=params)
    data = response.json()
    if 'Type' in data:
        if data['Type']=='ERROR':
            print(data['Content'])
            return
    #print(json.dumps(data, indent=4))
    
    logger.info('finished reading data')
    
    csv = data['Object']['Content']
    #structure = data['Object']['Structure']
    #print(json.dumps(structure, indent=4))

    df = pd.read_csv(StringIO(csv),
                     engine='python',
                     skiprows=6,
                     skipfooter=3,
                     header=None,
                     names=['JAHR',
                            'QUARTG',
                            'VST074',          # Real wage index
                            'VST074_CH0004',   # Change on previous year's month
                            'VST075',          # Nominal wage index
                            'VST075_CH0006'],  # Change on previous month
                     na_values=['.', '...', '-'],
                     sep=';', decimal=',')
    df = df[df['VST074'].notna()].fillna(0)
    
    quartal = []
    dates = []
    for index,row in df.iterrows():
        temp = row.QUARTG.split(' ')
        quartal.append(int(temp[1]))
        dates.append(f'{row.JAHR}-Q{int(temp[1])}')
    df['QUARTG'] = quartal
    df['DATUM'] = pd.PeriodIndex(dates, freq='Q').to_timestamp() + pd.DateOffset(months=3, days=-1)
    
    columnMap = {
        'DATUM':         ('Date', 'date'),
        'JAHR':          ('Year', 'integer'),
        'QUARTG':        ('Quarter', 'integer'),
        'VST074':        ('Real wage index', 'number'),
        'VST074_CH0004': ("Change on previous year's quarter", 'number'),
        'VST075':        ('Nominal wage index', 'number'),
        'VST075_CH0006': ("Change on previous year's quarter", 'number')
    }

    df = df[['DATUM',
             'JAHR',
             'QUARTG',
             'VST074',
             'VST074_CH0004',
             'VST075',
             'VST075_CH0006']]
    
    df.to_csv('rwi_quarterly.csv',
              encoding='utf-8', mode='w', index=False)
        
    logger.info('wrote data file')
    
    fields = []
    for i,c in enumerate(df.columns):
        field = {}
        field['name'] = c
    
        cd = columnMap[c]
        field['type'] = cd[1]
        if cd[1]=='date':
            field['format'] = '%Y-%m-%d'
        field['description'] = cd[0]
        
        fields.append(field)
    schema = { 'fields': fields }
    
    with open('schema_rwi_quarterly.json', 'w') as f:
        f.write(json.dumps(schema, indent=2))
        f.close()
    
    logger.info('wrote schema file')

if __name__ == "__main__":
    
    configParser = configparser.RawConfigParser()
    configParser.read(os.path.expanduser('~/.accounts.ini'))

    token = configParser['Destatis']['token']

    updateRealWageIndex(token)
    