#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: Andreas Mussgiller
"""

import locale
locale.setlocale(locale.LC_ALL, 'en_US.UTF-8')
import calendar

import os
import configparser
import json

import logging

import datetime

import requests

from io import StringIO
import pandas as pd

def updateConsumerPriceIndex(token):

    logging.basicConfig(format="{asctime} [{levelno}:{name}]: {message}",
                        style="{",
                        datefmt="%Y-%m-%d %H:%M",
                        level=logging.INFO)
    logger = logging.getLogger()
    
    monthmap = { month: index for index, month in enumerate(calendar.month_name) if month }

    logger.info('reading catalogue/tables for 61111-0002')
    
    url = 'https://www-genesis.destatis.de/genesisWS/rest/2020/catalogue/tables'
    params = dict(
        username = token,
        selection = '61111-0002',
        area = 'all',
        pagelength = '20',
        compress = 'false',
        language = 'en'
    )
    
    response = requests.get(url=url, params=params)
    data = response.json()
    if 'Type' in data:
        if data['Type']=='ERROR':
            print(data['Content'])
            return
    #print(json.dumps(data, indent=4))

    entry = data['List'][0]
    match = entry['Time'].split(' to ')
    dtStart = datetime.datetime.strptime(match[0].strip(), "%B %Y").date()
    dtEnd = datetime.datetime.strptime(match[1].strip(), "%B %Y").date()
    
    logger.info(f'range: {dtStart} - {dtEnd}')
    
    logger.info('reading data/table for 61111-0002')
    
    url = 'https://www-genesis.destatis.de/genesisWS/rest/2020/data/table'
    params = dict(
        username = token,
        name = '61111-0002',
        startyear = f'{dtStart.year}',
        endyear = f'{dtEnd.year}',
        job = 'false',
        values = 'true',
        metadata = 'true',
        area = 'all',
        compress = 'false',
        language = 'en'
    )
    
    response = requests.get(url=url, params=params)
    data = response.json()
    if 'Type' in data:
        if data['Type']=='ERROR':
            print(data['Content'])
            return
    #print(json.dumps(data, indent=4))
    
    logger.info('finished reading data')
    
    csv = data['Object']['Content']
    #structure = data['Object']['Structure']
    #print(json.dumps(structure, indent=4))

    df = pd.read_csv(StringIO(csv),
                 engine='python',
                 skiprows=4,
                 skipfooter=3,
                 header=[0,1],
                 na_values=['.', '...', '-'],
                 sep=';', decimal=',')
    df.columns = df.columns.map('|'.join).str.strip('|')

    match = df.columns.values[2].split('|')
    logger.info(f'reference for consumer price index: {match[1]}')

    df.columns.values[0] = 'JAHR'
    df.columns.values[1] = 'MONAT'
    df.columns.values[2] = 'PREIS1'        # Consumer price index
    df.columns.values[3] = 'PREIS1_CH0004' # Change on previous year's month
    df.columns.values[4] = 'PREIS1_CH0005' # Change on previous month

    columnMap = {
        'DATUM':  ('Date', 'date'),
        'PREIS1': ('Consumer price index', 'number'),
        'PREIS1_CH0004': ("Change on previous year's month", 'number'),
        'PREIS1_CH0005': ('Change on previous month', 'number')
    }
    
    df = df[df['PREIS1'].notna()].fillna(0)
    
    df['TAG'] = 1
    iMonat = []
    for index,row in df.iterrows():
        iMonat.append(monthmap[row.MONAT])
    df['DATUM'] = pd.to_datetime(dict(year=df.JAHR,
                                      month=iMonat,
                                      day=df.TAG))
    
    df = df[['DATUM',
             'PREIS1',
             'PREIS1_CH0004',
             'PREIS1_CH0005']]
    
    df.to_csv('cpi_monthly.csv',
              encoding='utf-8', mode='w', index=False)
        
    logger.info('wrote data file')
    
    fields = []
    for i,c in enumerate(df.columns):
        field = {}
        field['name'] = c
    
        cd = columnMap[c]
        field['type'] = cd[1]
        if cd[1]=='date':
            field['format'] = '%Y-%m-%d'
        field['description'] = cd[0]
        
        fields.append(field)
    schema = { 'fields': fields }
    
    with open('schema_cpi_monthly.json', 'w') as f:
        f.write(json.dumps(schema, indent=2))
        f.close()
    
    logger.info('wrote schema file')

if __name__ == "__main__":
    
    configParser = configparser.RawConfigParser()
    configParser.read(os.path.expanduser('~/.accounts.ini'))

    token = configParser['Destatis']['token']

    updateConsumerPriceIndex(token)
    