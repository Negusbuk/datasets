#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: Andreas Mussgiller
"""

import locale
locale.setlocale(locale.LC_ALL, 'en_US.UTF-8')

import os
import configparser

from update_cpi_monthly import updateConsumerPriceIndex
from update_gdp_quarterly import updateGDPQuarterly
from update_rwi_quarterly import updateRealWageIndex
from update_wages_quarterly import updateWagesQuarterly

if __name__ == "__main__":
    
    configParser = configparser.RawConfigParser()
    configParser.read(os.path.expanduser('~/.accounts.ini'))

    token = configParser['Destatis']['token']

    updateConsumerPriceIndex(token)
    updateGDPQuarterly(token)
    updateWagesQuarterly(token)
    updateRealWageIndex(token)