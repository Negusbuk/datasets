#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: Andreas Mussgiller
"""

import locale
locale.setlocale(locale.LC_ALL, 'en_US.UTF-8')
import calendar

import os
import configparser
import json

import logging

import datetime

import requests

from io import StringIO
import pandas as pd

def updateWagesQuarterly(token):

    logging.basicConfig(format="{asctime} [{levelno}:{name}]: {message}",
                        style="{",
                        datefmt="%Y-%m-%d %H:%M",
                        level=logging.INFO)
    logger = logging.getLogger()
    
    logger.info('reading catalogue/tables for 81000-0018')

    url = 'https://www-genesis.destatis.de/genesisWS/rest/2020/catalogue/tables'
    params = dict(
        username = token,
        selection = '81000-0018',
        area = 'all',
        pagelength = '20',
        compress = 'false',
        language = 'en'
    )
    
    response = requests.get(url=url, params=params)
    data = response.json()
    if 'Type' in data:
        if data['Type']=='ERROR':
            print(data['Content'])
            return
    #print(json.dumps(data, indent=4))

    entry = data['List'][0]
    match = entry['Time'].split(' to ')
    temp = match[0].strip().lstrip('Quarter').strip().split(' ')
    quarterStart = int(float(temp[0].strip()))
    yearStart = int(float(temp[1].strip()))
    temp = match[1].strip().lstrip('Quarter').strip().split(' ')
    quarterEnd = int(float(temp[0].strip()))
    yearEnd = int(float(temp[1].strip()))
    
    logger.info(f'range: Q{quarterStart}/{yearStart} - Q{quarterEnd}/{yearEnd}')
    
    logger.info('reading data/table for 81000-0018')
    
    url = 'https://www-genesis.destatis.de/genesisWS/rest/2020/data/table'
    params = dict(
        username = token,
        name = '81000-0018',
        transpose = 'true',
        startyear = f'{yearStart}',
        endyear = f'{yearEnd}',
        job = 'false',
        values = 'true',
        metadata = 'true',
        area = 'all',
        compress = 'false',
        language = 'en',
        contents = 'WIR001,WIR002,PRO015,PRO016,KOS018,KOS019,VST038,VST039,KOS020,KOS021',
        classifyingvariable1 = 'WERT08',
        classifyingkey1 = 'WERTORG',
        #classifyingvariable2 = 'VGRPB5',
        #classifyingkey2 = 'VGRJPM,VGRPKM',
    )
    
    response = requests.get(url=url, params=params)
    data = response.json()
    if 'Type' in data:
        if data['Type']=='ERROR':
            print(data['Content'])
            return
    #print(json.dumps(data, indent=4))
    
    logger.info('finished reading data')
    
    csv = data['Object']['Content']
    #structure = data['Object']['Structure']
    #print(json.dumps(structure, indent=4))
    
    df = pd.read_csv(StringIO(csv),
                 engine='python',
                 skiprows=6,
                 header=[0,1],
                 skipfooter=4,
                 na_values=['.', '...', '-'],
                 sep=';', decimal=',')
    df.columns = df.columns.map('|'.join).str.strip('|')
    
    columnMap = {
        'DATUM':  ('Date (end of quarter)', 'date'),
        'JAHR':   ('Year', 'integer'),
        'QUARTG': ('Quarter', 'integer'),
        'WERT08': ('Original and adjusted data', 'string'),
        'WZ08G2': ('Industries in national accounts', 'string'),
        'WIR001': ('GVA at current prices p.pers. in empl. (dom.conc.)', 'number'),
        'WIR002': ('GVA at cur.pr. p.h.wor.by pers.in empl.(dom.conc.)', 'number'),
        'PRO015': ('Lab.prod. per person in empl. (domestic concept)', 'number'),
        'PRO016': ('Lab.prod. per h.worked by pers.in empl.(dom.conc.)', 'number'),
        'KOS018': ('Compens. of empl. per employee (domestic concept)', 'number'),
        'KOS019': ('Compens. of empl. per employee hour (dom. concept)', 'number'),
        'VST038': ('Gross wages and salaries per employee (dom. conc.)', 'number'),
        'VST039': ('Gross wages and sal. per empl. hour (dom. concept)', 'number'),
        'KOS020': ('Unit labour costs per capita (domestic concept)', 'number'),
        'KOS021': ('Unit labour costs per hour (domestic concept)', 'number'),
    }

    df.columns.values[0] =  'WERT08'
    df.columns.values[1] =  'WZ08G2'
    df.columns.values[2] =  'JAHR'
    df.columns.values[3] =  'QUARTG'
    df.columns.values[4] =  'WIR001' # GVA at current prices p.pers. in empl. (dom.conc.)
    df.columns.values[5] =  'WIR002' # GVA at cur.pr. p.h.wor.by pers.in empl.(dom.conc.)
    df.columns.values[6] =  'PRO015' # Lab.prod. per person in empl. (domestic concept)
    df.columns.values[7] =  'PRO016' # Lab.prod. per h.worked by pers.in empl.(dom.conc.)
    df.columns.values[8] =  'KOS018' # Compens. of empl. per employee (domestic concept)
    df.columns.values[9] =  'KOS019' # Compens. of empl. per employee hour (dom. concept)
    df.columns.values[12] = 'VST038' # Gross wages and salaries per employee (dom. conc.)
    df.columns.values[13] = 'VST039' # Gross wages and sal. per empl. hour (dom. concept)
    df.columns.values[10] = 'KOS020' # Unit labour costs per capita (domestic concept)
    df.columns.values[11] = 'KOS021' # Unit labour costs per hour (domestic concept)
    
    df = df[df['WIR001'].notna()].fillna(0)
    
    df.loc[df['WERT08']=='Originalwerte', 'WERT08'] = 'WERTORG'

    quartal = []
    dates = []
    for index,row in df.iterrows():
        temp = row.QUARTG.split(' ')
        quartal.append(int(temp[1]))
        dates.append(f'{row.JAHR}-Q{int(temp[1])}')
    df['QUARTG'] = quartal
    df['DATUM'] = pd.PeriodIndex(dates, freq='Q').to_timestamp() + pd.DateOffset(months=3, days=-1)
    
    df = df[['DATUM',
             'JAHR',
             'QUARTG',
             'WERT08',
             'WZ08G2',
             'WIR001',
             'WIR002',
             'PRO015',
             'PRO016',
             'KOS018',
             'KOS019',
             'VST038',
             'VST039',
             'KOS020',
             'KOS021']]

    df.to_csv('wages_quarterly.csv',
              encoding='utf-8', mode='w', index=False)
        
    logger.info('wrote data file')
    
    fields = []
    for i,c in enumerate(df.columns):
        field = {}
        field['name'] = c
    
        cd = columnMap[c]
        field['type'] = cd[1]
        if cd[1]=='date':
            field['format'] = '%Y-%m-%d'
        field['description'] = cd[0]
        
        fields.append(field)
    schema = { 'fields': fields }
    
    with open('schema_wages_quarterly.json', 'w') as f:
        f.write(json.dumps(schema, indent=2))
        f.close()
    
    logger.info('wrote schema file')

if __name__ == "__main__":
    
    configParser = configparser.RawConfigParser()
    configParser.read(os.path.expanduser('~/.accounts.ini'))

    token = configParser['Destatis']['token']

    updateWagesQuarterly(token)
