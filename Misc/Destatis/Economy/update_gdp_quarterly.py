#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: Andreas Mussgiller
"""

import locale
locale.setlocale(locale.LC_ALL, 'en_US.UTF-8')
import calendar

import os
import configparser
import json

import logging

import datetime

import requests

from io import StringIO
import pandas as pd

def updateGDPQuarterly(token):
    
    logging.basicConfig(format="{asctime} [{levelno}:{name}]: {message}",
                        style="{",
                        datefmt="%Y-%m-%d %H:%M",
                        level=logging.INFO)
    logger = logging.getLogger()
    
    logger.info('reading catalogue/tables for 81000-0002')
    
    url = 'https://www-genesis.destatis.de/genesisWS/rest/2020/catalogue/tables'
    params = dict(
        username = token,
        selection = '81000-0002',
        area = 'all',
        pagelength = '20',
        compress = 'false',
        language = 'en'
    )
    
    response = requests.get(url=url, params=params)
    data = response.json()
    if 'Type' in data:
        if data['Type']=='ERROR':
            print(data['Content'])
            return
    #print(json.dumps(data, indent=4))

    entry = data['List'][0]
    match = entry['Time'].split(' to ')
    temp = match[0].strip().lstrip('Quarter').strip().split(' ')
    quarterStart = int(float(temp[0].strip()))
    yearStart = int(float(temp[1].strip()))
    temp = match[1].strip().lstrip('Quarter').strip().split(' ')
    quarterEnd = int(float(temp[0].strip()))
    yearEnd = int(float(temp[1].strip()))
    
    logger.info(f'range: Q{quarterStart}/{yearStart} - Q{quarterEnd}/{yearEnd}')
    
    logger.info('reading data/table for 81000-0002')

    url = 'https://www-genesis.destatis.de/genesisWS/rest/2020/data/table'
    params = dict(
        username = token,
        name = '81000-0002',
        transpose = 'true',
        contents = 'BWS001,VGR014,BIP005,BIP004',
        classifyingvariable1 = 'WERT05',
        classifyingkey1 = 'WERTORG',
        classifyingvariable2 = 'VGRPB5',
        classifyingkey2 = 'VGRJPM,VGRPKM,VGRPVK,VGRPVU',
        startyear = f'{yearStart}',
        endyear = f'{yearEnd}',
        job = 'false',
        values = 'true',
        metadata = 'true',
        area = 'all',
        compress = 'false',
        language = 'en'
    )
    
    response = requests.get(url=url, params=params)
    data = response.json()
    if 'Type' in data:
        if data['Type']=='ERROR':
            print(data['Content'])
            return
    #print(json.dumps(data, indent=4))
    
    logger.info('finished reading data')
    
    csv = data['Object']['Content']
    #structure = data['Object']['Structure']
    #print(json.dumps(structure, indent=4))

    df = pd.read_csv(StringIO(csv),
                 engine='python',
                 skiprows=6,
                 header=[0,1],
                 skipfooter=4,
                 na_values=['.', '...', '-'],
                 sep=';', decimal=',')
    df.columns = df.columns.map('|'.join).str.strip('|')
    
    columnMap = {
        'DATUM':  ('Date (end of quarter)', 'date'),
        'JAHR':   ('Year', 'integer'),
        'QUARTG': ('Quarter', 'integer'),
        'WERT05': ('Original and adjusted data', 'string'),
        'VGRPB5': ('Price base (current prices / price-adjusted)', 'string'),
        'BWS001': ('Gross value added', 'number'),
        'VGR014': ('Gross domestic product', 'number'),
        'BIP005': ('Gross domestic product (change in %)', 'number'),
        'BIP004': ('Gross domestic product per inhabitant', 'number')
    }

    df.columns.values[0] = 'WERT05'
    df.columns.values[1] = 'VGRPB5'
    df.columns.values[2] = 'JAHR'
    df.columns.values[3] = 'QUARTG'
    df.columns.values[4] = 'BWS001'
    df.columns.values[5] = 'VGR014'
    df.columns.values[6] = 'BIP005'
    df.columns.values[7] = 'BIP004'
    
    df = df[df['BWS001'].notna()].fillna(0)
    
    df.loc[df['WERT05']=='Unadjusted values', 'WERT05'] = 'WERTORG'
    df.loc[df['VGRPB5']=='At current prices (bn EUR)', 'VGRPB5'] = 'VGRJPM'
    df.loc[df['VGRPB5']=='Price-adjusted, chain index (2020=100)', 'VGRPB5'] = 'VGRPKM'
    df.loc[df['VGRPB5']=='Price-adjusted, chain-linked volume data (bn EUR)', 'VGRPB5'] = 'VGRPVK'
    df.loc[df['VGRPB5']=='Price-adjusted, unlinked volume data (bn EUR)', 'VGRPB5'] = 'VGRPVU'
    
    quartal = []
    dates = []
    for index,row in df.iterrows():
        temp = row.QUARTG.split(' ')
        quartal.append(int(temp[1]))
        dates.append(f'{row.JAHR}-Q{int(temp[1])}')
    df['QUARTG'] = quartal
    df['DATUM'] = pd.PeriodIndex(dates, freq='Q').to_timestamp() + pd.DateOffset(months=3, days=-1)
    
    df = df[['DATUM',   # Date
             'JAHR',    # Year
             'QUARTG',  # Quarters
             'WERT05',  # Original and adjusted data
             'VGRPB5',  # Price base (current prices / price-adjusted)
             'BWS001',  # Gross value added
             'VGR014',  # Gross domestic product
             'BIP005',  # Gross domestic product (change in %)
             'BIP004']] # Gross domestic product per inhabitant
    
    df.to_csv('gdp_quarterly.csv',
              encoding='utf-8', mode='w', index=False)
            
    logger.info('wrote data file')
    
    fields = []
    for i,c in enumerate(df.columns):
        field = {}
        field['name'] = c
    
        cd = columnMap[c]
        field['type'] = cd[1]
        if cd[1]=='date':
            field['format'] = '%Y-%m-%d'
        field['description'] = cd[0]
        
        fields.append(field)
    schema = { 'fields': fields }
    
    with open('schema_gdp_quarterly.json', 'w') as f:
        f.write(json.dumps(schema, indent=2))
        f.close()
    
    logger.info('wrote schema file')

if __name__ == "__main__":
    
    configParser = configparser.RawConfigParser()
    configParser.read(os.path.expanduser('~/.accounts.ini'))

    token = configParser['Destatis']['token']

    updateGDPQuarterly(token)
