import pandas as pd
import numpy as np
import json

strukturdaten_df = pd.read_csv('archive/btw21_strukturdaten.csv',
  encoding='utf-8', sep=';',
  thousands='.', decimal=',',
  skiprows=8)

strukturdaten_df.fillna(0, inplace=True)
strukturdaten_df.drop('Fußnoten', axis=1, inplace=True)
strukturdaten_df.drop(strukturdaten_df[strukturdaten_df['Wahlkreis-Nr.'] >= 900].index, inplace=True)

strukturdaten_df.rename(columns={
  'Gemeinden am 31.12.2019 (Anzahl)': 'Anzahl Gemeinden',
  'Fläche am 31.12.2019 (km²)': 'Fläche [km²]',
  'Bevölkerung am 31.12.2019 - Insgesamt (in 1000)': 'Bevölkerung [1000 EW]',
  'Bevölkerung am 31.12.2019 - Deutsche (in 1000)': 'Bevölkerung Deutsche [1000 EW]',
  'Bevölkerung am 31.12.2019 - Ausländer/-innen (%)': 'Anteil Ausländer/-innen [%]',
  'Bevölkerungsdichte am 31.12.2019 (EW je km²)': 'Bevölkerungsdichte [EW je km²]',
  'Zu- (+) bzw. Abnahme (-) der Bevölkerung 2019 - Geburtensaldo (je 1000 EW)': 'Geburtensaldo [je 1000 EW]',
  'Zu- (+) bzw. Abnahme (-) der Bevölkerung 2019 - Wanderungssaldo (je 1000 EW)': 'Wanderungssaldo [je 1000 EW]',
  'Alter von ... bis ... Jahren am 31.12.2019 - unter 18 (%)': 'Bevölkerungsstruktur - unter 18 [%]',
  'Alter von ... bis ... Jahren am 31.12.2019 - 18-24 (%)': 'Bevölkerungsstruktur - 18-24 [%]',
  'Alter von ... bis ... Jahren am 31.12.2019 - 25-34 (%)': 'Bevölkerungsstruktur - 25-34 [%]',
  'Alter von ... bis ... Jahren am 31.12.2019 - 35-59 (%)': 'Bevölkerungsstruktur - 35-59 [%]',
  'Alter von ... bis ... Jahren am 31.12.2019 - 60-74 (%)': 'Bevölkerungsstruktur - 60-74 [%]',
  'Alter von ... bis ... Jahren am 31.12.2019 - 75 und mehr (%)': 'Bevölkerungsstruktur - 75 und mehr [%]',
  'Bodenfläche nach Art der tatsächlichen Nutzung am 31.12.2019 - Siedlung und Verkehr (%)': 'Bodennutzung - Siedlung und Verkehr [%]',
  'Bodenfläche nach Art der tatsächlichen Nutzung am 31.12.2019 - Vegetation und Gewässer (%)': 'Bodennutzung - Vegetation und Gewässer [%]',
  'Fertiggestellte Wohnungen 2019 (je 1000 EW)': 'Fertiggestellte Wohnungen [je 1000 EW]',
  'Bestand an Wohnungen am 31.12.2019 - insgesamt (je 1000 EW)': 'Anzahl Wohnungen [je 1000 EW]',
  'Wohnfläche am 31.12.2019 (je Wohnung)': 'Wohnfläche [je Whg.]',
  'Wohnfläche am 31.12.2019 (je EW)': 'Wohnfläche [je EW]',
  'PKW-Bestand am 01.01.2020 - PKW insgesamt (je 1000 EW)': 'PKW-Bestand - Insgesamt [je 1000 EW]',
  'PKW-Bestand am 01.01.2020 - PKW mit Elektro- oder Hybrid-Antrieb (%)': 'PKW-Bestand - Elektro-/Hybrid-Antrieb [%]',
  'Arbeitslosenquote Februar 2021 - insgesamt': 'Arbeitslosenquote - Insgesamt',
  'Arbeitslosenquote Februar 2021 - Männer': 'Arbeitslosenquote - Männer',
  'Arbeitslosenquote Februar 2021 - Frauen': 'Arbeitslosenquote - Frauen',
  'Arbeitslosenquote Februar 2021 - 15 bis 24 Jahre': 'Arbeitslosenquote - 15 - 24 Jahre',
  'Arbeitslosenquote Februar 2021 - 55 bis 64 Jahre': 'Arbeitslosenquote - 55 - 64 Jahre'
}, inplace=True)

#print(strukturdaten_df.head())

strukturdaten_df.to_csv('strukturdaten.csv', encoding='utf-8', mode='w', index=False)

fields = [ ]

for r in strukturdaten_df.columns.values:
  fields.append({ 'name': r, 'type': 'number'})

fields[0]['type'] = 'string'
fields[2]['type'] = 'string'

schema = { 'fields': fields }

#print(json.dumps(schema, indent=2))

with open('schema_strukturdaten.json', 'w') as f:
  f.write(json.dumps(schema, indent=2))
  f.close()


kerg2_df = pd.read_csv('archive/kerg2.csv',
  encoding='utf-8', sep=';',
  thousands='.', decimal=',',
  skiprows=9, header=0)
kerg2_df.fillna(0, inplace=True)

kerg2_df.drop(kerg2_df[kerg2_df['Gebietsart'] == 'Bund'].index, inplace=True)
kerg2_df.drop(kerg2_df[kerg2_df['Gebietsart'] == 'Land'].index, inplace=True)

kerg2_df.drop('Wahlart', axis=1, inplace=True)
kerg2_df.drop('Wahltag', axis=1, inplace=True)
kerg2_df.drop('Bemerkung', axis=1, inplace=True)

#print(kerg2_df.head(10))

parteien = kerg2_df['Gruppenname'].unique().tolist()
parteien.pop(0)
parteien.pop(0)
parteien.pop(0)
parteien.pop(0)

parteien = ['CDU', 'CSU', 'SPD', 'FDP', 'GRÜNE', 'AfD', 'DIE LINKE',
  'Die PARTEI', 'FREIE WÄHLER', 'NPD', 'ÖDP', 'MLPD', 'dieBasis']

#print(parteien)

sub = kerg2_df.query('Gruppenname=="Wahlberechtigte"')
data_df = pd.DataFrame({
  'WKR_NR': sub['Gebietsnummer'].astype('int64'),
  'WKR_NAME': sub['Gebietsname'],
  'Wahlberechtigte': sub['Anzahl'].values})

sub = kerg2_df.query('Gruppenname=="Wählende"')
#print('{:s}: {:.0f}'.format('Wählende', len(sub)))
data_df['Wählende'] = sub['Anzahl'].values

columns = ['Ungültige', 'Gültige']

for c in columns:
  sub = kerg2_df.query('Gruppenname=="{:s}" and Stimme==1.0'.format(c))
  #print('{:s} 1: {:.0f}'.format(c, len(sub)))
  data_df['{:s} 1'.format(c)] = sub['Anzahl'].values
  
  sub = kerg2_df.query('Gruppenname=="{:s}" and Stimme==2.0'.format(c))
  #print('{:s} 2: {:.0f}'.format(c, len(sub)))
  data_df['{:s} 2'.format(c)] = sub['Anzahl'].values
  
for p in parteien:
  sub = kerg2_df.query('Gruppenname=="{:s}" and Stimme==1.0'.format(p))
  #print('{:s} 1: {:.0f}'.format(p, len(sub)))
  temp = pd.DataFrame({
    'WKR_NR': sub['Gebietsnummer'].astype('int64'),
    '{:s} 1'.format(p): sub['Anzahl'].values})
  temp['{:s} 1'.format(p)] = temp['{:s} 1'.format(p)]
  data_df = pd.merge(left=data_df, left_on='WKR_NR', right=temp, right_on='WKR_NR', how='outer')
  
  sub = kerg2_df.query('Gruppenname=="{:s}" and Stimme==2.0'.format(p))
  #print('{:s} 2: {:.0f}'.format(p, len(sub)))
  temp = pd.DataFrame({
    'WKR_NR': sub['Gebietsnummer'].astype('int64'),
    '{:s} 2'.format(p): sub['Anzahl'].values})
  data_df = pd.merge(left=data_df, left_on='WKR_NR', right=temp, right_on='WKR_NR', how='outer')
  
data_df.to_csv('ergebnisse.csv', encoding='utf-8', mode='w', index=False)

#print(data_df.head())

fields = [ ]

for r in data_df.columns.values:
  fields.append({
    'name': r,
    'type': 'number'})
  
fields[0]['type'] = 'integer'
fields[1]['type'] = 'string'

schema = { 'fields': fields }

#print(json.dumps(schema, indent=2))

with open('schema_ergebnisse.json', 'w') as f:
  f.write(json.dumps(schema, indent=2))
  f.close()
  