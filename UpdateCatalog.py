import sys
import os
import os.path
import importlib.util
import subprocess

if importlib.util.find_spec('frictionless') is None:
    subprocess.call([sys.executable, '-m', 'pip', 'install', 'frictionless'])
    subprocess.call([sys.executable, '-m', 'pip', 'install', 'frictionless[pandas]'])

from frictionless import Catalog, Dataset, Package, Resource

packages = []

for dirpath, dirnames, filenames in os.walk("."):
    
    if dirpath.endswith('.git'):
        continue

    if dirpath.endswith('.ipynb_checkpoints'):
        continue    

    if dirpath.endswith('.'):
        continue

    if dirpath.startswith('./External'):
        continue

    for filename in [f for f in filenames if f.endswith("datapackage.json")]:
        packages.append(os.path.join(dirpath, filename))

for dirpath, dirnames, filenames in os.walk("./External"):
    
    if dirpath.endswith('.git'):
        continue

    if dirpath.endswith('.ipynb_checkpoints'):
        continue

    if dirpath.endswith('.'):
        continue

    for filename in [f for f in filenames if f.endswith("datapackage.json")]:
        packages.append(os.path.join(dirpath, filename))

catalog = Catalog()
for p in packages:
    package = Package(p)
    #print(package.name)
    dataset = Dataset(name=package.name, package=package)
    catalog.add_dataset(dataset)

f = open('datacatalog.json', 'w')
f.write(catalog.to_json())
f.close()

