import sys
import importlib.util
import subprocess
import textwrap

if importlib.util.find_spec('frictionless') is None:
    subprocess.call([sys.executable, '-m', 'pip', 'install', 'frictionless'])
    subprocess.call([sys.executable, '-m', 'pip', 'install', 'frictionless[pandas]'])

from frictionless import Catalog, Package, Resource

catalog = Catalog('./datacatalog.json')

for ds in catalog.datasets:
    p = ds._package
    print(p.name + ': ' + p.title)
    print('------------------------------')
    for r in p.resources:
        if r.title is not None:
            print('  ' + r.name + ': ' + r.title)
        else:
            print('  ' + r.name)
        if r.description is not None:
            lines = textwrap.wrap(r.description,
                                  width=60,)
            for l in lines:
                print('      ' + l)
    