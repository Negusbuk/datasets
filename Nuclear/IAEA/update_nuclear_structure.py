import numpy as np
import pandas as pd
import urllib.request

# source: https://www-nds.iaea.org/relnsd/vcharthtml/VChartHTML.html

livechart = 'https://nds.iaea.org/relnsd/v0/data?'

req = urllib.request.Request(livechart + 'fields=ground_states&nuclides=all')
req.add_header('User-Agent', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:77.0) Gecko/20100101 Firefox/77.0')
df = pd.read_csv(urllib.request.urlopen(req))

df.drop(df[df.z==0].index, inplace=True)

df['z'] = df.z.astype(int)
df['n'] = df.n.astype(int)
df['a'] = df.z + df.n

df['abundance'] = df.abundance.replace(' ', 0.0).astype(float)
df['half_life_sec'] = df.half_life_sec.replace(' ', np.inf).astype(float)
df = df[pd.to_numeric(df['binding'], errors='coerce').notna()]
df['binding'] = df.binding.replace(' ', np.nan).astype(float)
df['atomic_mass'] = df.atomic_mass.replace(' ', np.inf).astype(float)

df = df[['z', 'n', 'a', 'symbol', 'abundance', 'half_life_sec', 'binding', 'atomic_mass']]

df.to_csv('nuclear_structure.csv', encoding='utf-8', mode='w', index=False)
